# Gravity: I2C LCD1602 RGB彩色背光液晶屏

![](./arduinoC/_images/featured.png)

# 积木

![](./arduinoC/_images/blocks.png)

# 程序实例

![](./arduinoC/_images/example.png)

![](./arduinoC/_images/example1.png)

# 支持列表

|主板型号|实时模式|ArduinoC|MicroPython|备注|
|-----|-----|:-----:|-----|-----|
|UNO||√|||
|Micro:Bit||√|||
|mpython||√|||
|Nano||√|||
|Leonardo||√|||
|Mega2560||√|||
 
 


# 更新日志

V0.0.1 基础功能完成

