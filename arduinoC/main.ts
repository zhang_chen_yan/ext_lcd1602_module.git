//% color="#00A600" iconWidth=50 iconHeight=40
namespace LCD1602{
    //% block="LCD1602 init  RS [RS]  EN [EN]    D4 [D4]   D5 [D5]  D6 [D6]   D7 [D7]" blockType="command"
    //% RS.shadow="dropdownRound"   RS.options="RS"
    //% EN.shadow="dropdownRound"   EN.options="EN"    
    //% D4.shadow="dropdownRound"   D4.options="D4"        
    //% D5.shadow="dropdownRound"   D5.options="D5"    
    //% D6.shadow="dropdownRound"   D6.options="D6"
    //% D7.shadow="dropdownRound"   D7.options="D7"     
    export function LCD1602Init(parameter: any, block: any) {      
        let rs=parameter.RS.code;  
        let en=parameter.EN.code; 
        let d4=parameter.D4.code; 
        let d5=parameter.D5.code; 
        let d6=parameter.D6.code; 
        let d7=parameter.D7.code; 
     
        Generator.addInclude("LCD1602Init","#include <LiquidCrystal.h>");  
        Generator.addObject(`LCD1602Init`,"LiquidCrystal",`lcd(${rs}, ${en}, ${d4}, ${d5},${d6}, ${d7});`);
        Generator.addSetup(`LCD1602Init`,"lcd.begin(16, 2);");
    
    }

      //% block="LCD1602 Dir[LR]  " blockType="command" 
      //% LR.shadow="dropdown"  LR.options="LR"
      export function LCD1602InitLR(parameter: any, block: any) {
        let lr=parameter.LR.code; 
    
        Generator.addCode(`lcd.scrollDisplay${lr}();`);
     
    }
    //% block="LCD1602 Print Line [Y] Mess [SR]  " blockType="command" 
    //% Y.shadow="dropdown" Y.options="Y" 
    //% SR.shadow="string"  SR.defl="DFRobot"
    export function LCD1602Initprintline(parameter: any, block: any) {
       
       let y=parameter.Y.code;  
       let sr=parameter.SR.code;  
 
       Generator.addCode(`lcd.setCursor(0,${y});`);
       Generator.addCode(`lcd.print(${sr});`);

   }
    
     //% block="LCD1602 Print X [X]Y [Y]Mess [SR]  " blockType="command" 
     //% X.shadow="number" X.params.min="0"  X.params.max="16" X.defl="0"
     //% Y.shadow="number" Y.params.min="0"  Y.params.max="1" Y.defl="0"
     //% SR.shadow="string"  SR.defl="hello"
     export function LCD1602Initprint(parameter: any, block: any) {
        let x=parameter.X.code;
        let y=parameter.Y.code;  
        let sr=parameter.SR.code;  
        Generator.addCode(`lcd.setCursor(${x},${y});`);
        Generator.addCode(`lcd.print(${sr});`);

    }
   
     
    //% block="LCD1602 Clear  " blockType="command" 
    
    export function LCD1602Initclear(parameter: any, block: any) {
        
        Generator.addCode("lcd.clear();");
        

    }
}
